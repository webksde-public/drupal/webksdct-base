# webksdct Base

Basic styles and mixins for our webksdct theme.

## Versioning
We use branches for the major drupal version and tags for the minor repository versions.

So the logic is:
[Drupal Major Version].[Repository Version (breaking changes)].[Patches / Fixes / Improvements]

.
